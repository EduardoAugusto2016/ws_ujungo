-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 09-Set-2017 às 09:35
-- Versão do servidor: 10.1.10-MariaDB
-- PHP Version: 5.5.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ujungo`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `address`
--

CREATE TABLE `address` (
  `id` int(11) NOT NULL,
  `id_user` varchar(250) NOT NULL,
  `address` varchar(250) NOT NULL,
  `number` varchar(100) NOT NULL,
  `neighborhood` varchar(100) NOT NULL,
  `zipcode` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `date_create` varchar(100) NOT NULL,
  `date_update` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `address`
--

INSERT INTO `address` (`id`, `id_user`, `address`, `number`, `neighborhood`, `zipcode`, `city`, `state`, `country`, `date_create`, `date_update`) VALUES
(4, '1', 'luis jose dias neto', '140', 'vale das antas', '37704388', 'poços de caldas', 'minas gerais', 'Brazil', '2017-07-12 14:04:59', '2017-07-12 14:08:51');

-- --------------------------------------------------------

--
-- Estrutura da tabela `requestDelivery`
--

CREATE TABLE `requestDelivery` (
  `id` int(11) NOT NULL,
  `id_user` varchar(250) NOT NULL,
  `size` varchar(250) NOT NULL,
  `tittle` varchar(250) NOT NULL,
  `info` varchar(250) NOT NULL,
  `address` varchar(250) NOT NULL,
  `number` varchar(250) NOT NULL,
  `neighborhood` varchar(250) NOT NULL,
  `city` varchar(250) NOT NULL,
  `state` varchar(250) NOT NULL,
  `cep` varchar(250) NOT NULL,
  `addressee` varchar(250) NOT NULL,
  `date` varchar(250) NOT NULL,
  `value` varchar(250) NOT NULL,
  `image` varchar(255) NOT NULL,
  `date_log` varchar(50) NOT NULL,
  `accept` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `requestDelivery`
--

INSERT INTO `requestDelivery` (`id`, `id_user`, `size`, `tittle`, `info`, `address`, `number`, `neighborhood`, `city`, `state`, `cep`, `addressee`, `date`, `value`, `image`, `date_log`, `accept`) VALUES
(4, '1', 'pequeno', 'Entregar carro', 'na casa no joao que robou pao', 'sei nao procura', '140', 'vale de nao sei da onde', 'acre', 'sei la', '6666', 'joao que robou pao', '16/7/2017', '5', '8f32c2c1.jpg', '2017-08-16 20:31:02', ''),
(5, '1', 'pequeno', '321', '321', '321', '321', '321', '231', '231', '321', '32312', '16/7/2017', '321', '277ceec5.jpg', '2017-08-16 22:16:21', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL,
  `avatar` varchar(250) NOT NULL,
  `date` varchar(100) NOT NULL,
  `date_update` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `password`, `avatar`, `date`, `date_update`) VALUES
(1, 'duhh', 'duh@gmail.com', '9999999', 'nick06', 'naruto-icon-3.png', '2017-06-22 21:06:15', '2017-08-16 20:42:58');

-- --------------------------------------------------------

--
-- Estrutura da tabela `wantdelivery`
--

CREATE TABLE `wantdelivery` (
  `id` int(11) NOT NULL,
  `id_user` varchar(250) NOT NULL,
  `bagsize` varchar(50) NOT NULL,
  `tittle` varchar(250) NOT NULL,
  `info` varchar(250) NOT NULL,
  `date` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `date_log` varchar(50) NOT NULL,
  `traveltype` varchar(255) NOT NULL,
  `accept` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `wantdelivery`
--

INSERT INTO `wantdelivery` (`id`, `id_user`, `bagsize`, `tittle`, `info`, `date`, `city`, `country`, `date_log`, `traveltype`, `accept`) VALUES
(3, '1', 'médio', 'teste', 'teste', '8/8/2017', 'teste', 'teste', '2017-09-08 17:47:00', 'Internacional', ''),
(4, '1', 'grande', 'teste', 'teste', '8/8/2017', 'teste', 'teste', '2017-09-08 17:47:11', 'Nacional', ''),
(5, '1', 'enorme', 'teste', 'teste', '8/8/2017', 'teste', 'teste', '2017-09-08 17:51:29', 'Nacional', ''),
(6, '1', 'enorme', 'teste', 'teste', '9/8/2017', 'Poços de caldas', 'Brazil', '2017-09-09 07:07:32', 'Nacional', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `requestDelivery`
--
ALTER TABLE `requestDelivery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wantdelivery`
--
ALTER TABLE `wantdelivery`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `requestDelivery`
--
ALTER TABLE `requestDelivery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wantdelivery`
--
ALTER TABLE `wantdelivery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
